package main

import (
	"log"
	"time"
	"os"
	"bufio"
	"regexp"
	"strings"
	"fmt"
	"gitlab.com/tsuchina.ga/waaw/my-markdown-parser/setting"
)

func init() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	// locationの設定
	const location = "Asia/Tokyo"
	loc, err := time.LoadLocation(location)
	if err != nil {
		loc = time.FixedZone(location, 9*60*60)
	}
	time.Local = loc
}

func main() {
	file, err := os.Open("./contract.md")
	if err != nil {
		log.Fatalln(err)
	}

	//設定
	settings := setting.GetInstance()
	counter := map[string]int {
		"h1": 0,
		"h2": 0,
		"h3": 0,
		"h4": 0,
	}

	//struct
	doc := new(document)
	b := new(block)

	//regexp
	h1 := regexp.MustCompile(`^#\s`)
	h2 := regexp.MustCompile(`^##\s`)
	h3 := regexp.MustCompile(`^###\s`)
	h4 := regexp.MustCompile(`^####\s`)
	empty := regexp.MustCompile(`^\s*$`)
	nest := regexp.MustCompile(`^\s{4}`)

	s := bufio.NewScanner(file)
	for s.Scan() {
		str := s.Text()
		//nestチェック
		nestCount := 0
		for nest.MatchString(str) {
			nestCount += 1
			str = nest.ReplaceAllString(str, "")
		}

		if h4.MatchString(str) {
			counter["h4"]++
			str = h4.ReplaceAllString(str, "")
			str = strings.Trim(str, " ")
			b.name = "h4"
			b.text = fmt.Sprintf(settings.Format.H4, counter["h4"], str)
		} else if h3.MatchString(str) {
			counter["h3"]++
			counter["h4"] = 0
			str = h3.ReplaceAllString(str, "")
			str = strings.Trim(str, " ")
			b.name = "h3"
			b.text = fmt.Sprintf(settings.Format.H3, counter["h3"], str)
		} else if h2.MatchString(str) {
			counter["h2"]++
			counter["h3"] = 0
			counter["h4"] = 0
			str = h2.ReplaceAllString(str, "")
			str = strings.Trim(str, " ")
			b.name = "h2"
			b.text = fmt.Sprintf(settings.Format.H2, counter["h2"], str)
		} else if h1.MatchString(str) {
			counter["h1"]++
			counter["h2"] = 0
			counter["h3"] = 0
			counter["h4"] = 0
			str = h1.ReplaceAllString(str, "")
			str = strings.Trim(str, " ")
			doc.title = str
			b.name = "h1"
			b.text = str
		} else if empty.MatchString(str) {
			if b.name != "" {
				doc.children = append(doc.children, *b)
				b = new(block)
			}
		} else {
			str = strings.Trim(str, " ")
			doc.children = append(doc.children, block{name: "p", text: str})
		}
	}

	doc.output()
}

type document struct {
	title    string
	children []block
}

func (doc *document) output() {
	file, err := os.Create(doc.title + ".html")
	if err != nil {
		log.Fatalln(err)
	}

	file.Write([]byte("<html>\n<head>\n"))
	file.Write([]byte(fmt.Sprintf("<title>%s</title>\n", doc.title)))
	file.Write([]byte("<link href=\"style.css\" rel=\"stylesheet\" type=\"text/css\">\n"))
	file.Write([]byte("</head>\n"))
	file.Write([]byte("<body>\n"))
	for _, child := range doc.children {
		child.output(file)
	}
	file.Write([]byte("</body>\n"))
}

type block struct {
	name     string
	text     string
	children []block
}

func (b *block) output(file *os.File) {
	str := ""
	//log.Println(b.name, b.text, len(b.children))

	if b.name != "" {
		str += fmt.Sprintf("<%s>", b.name)
		str += b.text
		if len(b.children) > 0 {
			for _, child := range b.children {
				child.output(file)
			}
		}
		str += fmt.Sprintf("</%s>", b.name)
	}

	str += "\n"
	file.Write([]byte(str))
}
