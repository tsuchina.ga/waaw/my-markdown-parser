# _WAAW10_ my-markdown-parser

毎週1つWebアプリを作ろうの10回目。

期間は18/07/13 - 18/07/19

Markdownで書かれたファイルを契約書の形にするとかそういう独自のパースをするための機能を作ってみる。


## ゴール

* [ ] Markdownをパースできる
* [ ] Markdownを独自の形にパースできる

## 目的

* [ ] Markdownのパーサーの処理を学ぶ
* [x] 最終的にはテキストファイルから契約書や仕様書を作る

## 課題

* [ ] markdownの標準仕様
    * [Markdown - Wikipedia](https://ja.wikipedia.org/wiki/Markdown)

* [ ] markdownのパース
    * [Markdownパーサの作成](https://blog.tiqwab.com/2016/12/22/md-parser.html)


## 振り返り

0. Idea:

    アイデアやテーマについて
    * PDFやMicrosoftOffice形式のバイナリファイルで管理するんじゃなくて、テキストファイルでの管理は間違いなく最高
    * markdownも直観的に分かりやすい記法なので悪くはないはず    

0. What went right:

    成功したこと・できたこと
    * テキストを読み込んで独自のルールでﾊﾟｰｽ
    * 設定ファイルを外だし

0. What went wrong:

    失敗したこと・できなかったこと
    * Markdownとしてのパースではなく、完全に独自ファイルの適当なパース

0. What I learned:

    学べたこと
    * Markdownパーサーの複雑さ

## サンプル

なし
