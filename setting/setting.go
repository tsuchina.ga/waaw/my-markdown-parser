package setting

import (
	"github.com/BurntSushi/toml"
	"log"
	"sync"
)

var settingFile = "./setting/setting.toml"
var settings *mySetting
var once sync.Once

func GetInstance() *mySetting {
	once.Do(func() {
		settings = new(mySetting)
		if err := Reload(); err != nil {
			log.Fatalln(err)
		}
	})
	return settings
}

func Reload() (err error) {
	_, err = toml.DecodeFile(settingFile, settings)
	return
}

type mySetting struct {
	Format formatSettings `toml:"format"`
}

type formatSettings struct {
	H2 string `toml:"h2"`
	H3 string `toml:"h3"`
	H4 string `toml:"h4"`
}
